import com.google.protobuf.ByteString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Serialization {
    private final List<BigDecimal> bigDecimalList;

    public Serialization(List<BigDecimal> bigDecimalList) {
        this.bigDecimalList = bigDecimalList;
    }

    public List<DecimalValueOuterClass.DecimalValue> serialize() {
        List<DecimalValueOuterClass.DecimalValue> serializedList = new ArrayList<>();
        for (BigDecimal bigDecimal : bigDecimalList) {
            serializedList.add(DecimalValueOuterClass.DecimalValue.newBuilder()
                    .setScale(bigDecimal.scale())
                    .setPrecision(bigDecimal.precision())
                    .setValue(ByteString.copyFrom(bigDecimal.unscaledValue().toByteArray()))
                    .build()
            );
        }
        return serializedList;
    }

    public List<BigDecimal> deserialize(List<DecimalValueOuterClass.DecimalValue> serialized) {
        List<BigDecimal> deserializedList = new ArrayList<>();
        for (DecimalValueOuterClass.DecimalValue serializedValue : serialized) {
            java.math.MathContext mc = new java.math.MathContext(serializedValue.getPrecision());
            deserializedList.add(new java.math.BigDecimal(
                    new java.math.BigInteger(serializedValue.getValue().toByteArray()),
                    serializedValue.getScale(),
                    mc)
            );
        }
        return deserializedList;
    }
}
