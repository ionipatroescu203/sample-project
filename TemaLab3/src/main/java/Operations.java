import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Operations {

    private final List<BigDecimal> bigDecimalList;

    public Operations(List<BigDecimal> bigDecimalList) {
        this.bigDecimalList = bigDecimalList;
    }

    public BigDecimal computeSum() {
        return bigDecimalList.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal computeAverage(RoundingMode roundingMode) {

        BigDecimal[] totalWithCount = bigDecimalList.stream()
                .filter(Objects::nonNull)
                .map(bd -> new BigDecimal[]{bd, BigDecimal.ONE})
                .reduce((a, b) -> new BigDecimal[]{a[0].add(b[0]), a[1].add(BigDecimal.ONE)})
                .get();

        return totalWithCount[0].divide(totalWithCount[1], roundingMode);
    }

    public List<BigDecimal> computePercent() {
        return bigDecimalList.stream()
                .sorted(Comparator.reverseOrder())
                .limit(bigDecimalList.size() / 10)
                .collect(Collectors.toList());
    }
}