import com.google.protobuf.ByteString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<BigDecimal> bigDecimalList = new ArrayList<>();

        java.math.BigDecimal bigDecimal = new java.math.BigDecimal("1234.56789");
        DecimalValueOuterClass.DecimalValue serialized = DecimalValueOuterClass.DecimalValue.newBuilder()
                .setScale(bigDecimal.scale())
                .setPrecision(bigDecimal.precision())
                .setValue(ByteString.copyFrom(bigDecimal.unscaledValue().toByteArray()))
                .build();

        java.math.MathContext mc = new java.math.MathContext(serialized.getPrecision());
        java.math.BigDecimal deserialized = new java.math.BigDecimal(
                new java.math.BigInteger(serialized.getValue().toByteArray()),
                serialized.getScale(),
                mc);

        System.out.println(bigDecimal.equals(deserialized));

        System.out.println("anda");

    }
}
