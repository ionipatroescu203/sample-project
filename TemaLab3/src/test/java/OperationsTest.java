
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class OperationsTest {
    Operations operations;

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        List<BigDecimal> bigDecimalsList = new ArrayList<>();
        for(int i = 1; i <= 1000; i++){
            bigDecimalsList.add(new BigDecimal(i));
        }
        operations = new Operations(bigDecimalsList);
    }

    @org.junit.jupiter.api.Test
    void computeSum() {
        assertEquals(new BigDecimal(500500), operations.computeSum());
    }

    @org.junit.jupiter.api.Test
    void computeAverage() {
        assertEquals(new BigDecimal(501), operations.computeAverage(RoundingMode.HALF_UP));
    }

    @org.junit.jupiter.api.Test
    void computePercent() {
        List<BigDecimal> testList = new ArrayList<>();
        for(int i = 1000; i > 900; i--){
            testList.add(new BigDecimal(i));
        }
        assertEquals(testList, operations.computePercent());
    }
}