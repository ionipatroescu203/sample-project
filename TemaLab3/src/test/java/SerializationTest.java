import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SerializationTest {

    @org.junit.jupiter.api.Test
    void smallTest() {
        List<BigDecimal> bigDecimalsList = new ArrayList<>();
        for (int i = 1; i <= 1000; i++) {
            bigDecimalsList.add(new BigDecimal(i));
        }
        Serialization serialization = new Serialization(bigDecimalsList);
        List<DecimalValueOuterClass.DecimalValue> serializedList = serialization.serialize();
        assertNotEquals(bigDecimalsList, serializedList);
        assertEquals(bigDecimalsList, serialization.deserialize(serializedList));
    }

    @org.junit.jupiter.api.Test
    void bigTest() {
        List<BigDecimal> bigDecimalsList = new ArrayList<>();
        for (long i = 1; i <= 100000000; i++) {
            bigDecimalsList.add(new BigDecimal(i % 10000));
        }
        Serialization serialization = new Serialization(bigDecimalsList);
        List<DecimalValueOuterClass.DecimalValue> serializedList = serialization.serialize();
        assertNotEquals(bigDecimalsList, serializedList);
        assertEquals(bigDecimalsList, serialization.deserialize(serializedList));
    }

}