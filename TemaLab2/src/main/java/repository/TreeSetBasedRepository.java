package repository;

import java.util.Set;
import java.util.TreeSet;

public class TreeSetBasedRepository<T> implements InMemoryRepository<T> {

    private final Set<T> set;

    public TreeSetBasedRepository() {
        this.set = new TreeSet<T>();
    }

    @Override
    public void add(T object) {
        set.add(object);
    }

    @Override
    public boolean contains(T object) {
        return set.contains(object);
    }

    @Override
    public void remove(T object) {
        set.remove(object);
    }
}
