package repository;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBasedRepository<T> implements InMemoryRepository<T>{
    private final Map<Integer, T> map;
    private int key;

    public ConcurrentHashMapBasedRepository() {
        this.map = new ConcurrentHashMap<Integer, T>();
        this.key = 0;
    }

    @Override
    public void add(T object) {
        this.map.put(this.key, object);
        this.key += 1;
    }

    @Override
    public boolean contains(T object) {
        return this.map.containsValue(object);
    }

    @Override
    public void remove(T object) {
        for (Map.Entry<Integer, T> entry : map.entrySet()){
            if(object.equals(entry.getValue())){
                map.remove(entry.getKey());
                break;
            }
        }
    }
}
