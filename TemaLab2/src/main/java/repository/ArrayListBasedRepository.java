package repository;


import java.util.ArrayList;
import java.util.List;

public class ArrayListBasedRepository<T> implements InMemoryRepository<T>{

    private final List<T> list;

    public ArrayListBasedRepository() {
        this.list = new ArrayList<T>();
    }

    @Override
    public void add(T object) {
        list.add(object);
    }

    @Override
    public boolean contains(T object) {
        return list.contains(object);
    }

    @Override
    public void remove(T object) {
        list.remove(object);
    }
}
