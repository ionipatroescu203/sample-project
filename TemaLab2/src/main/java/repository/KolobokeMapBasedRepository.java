package repository;

import com.koloboke.collect.map.hash.HashIntObjMaps;

import java.util.Map;

public class KolobokeMapBasedRepository<T> implements InMemoryRepository<T> {
    private Map<Integer, T> map;
    private int key;

    public KolobokeMapBasedRepository() {
        this.map = HashIntObjMaps.newMutableMap();
        this.key = 0;
    }

    @Override
    public void add(T object) {
        this.map.put(this.key, object);
        this.key += 1;
    }

    @Override
    public boolean contains(T object) {
        return this.map.containsValue(object);
    }

    @Override
    public void remove(T object) {
        for (Map.Entry<Integer, T> entry : map.entrySet()){
            if(object.equals(entry.getValue())){
                map.remove(entry.getKey());
                break;
            }
        }
    }
}
