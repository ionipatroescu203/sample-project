package repository;

import java.util.Collections;
import java.util.Map;

import it.unimi.dsi.fastutil.*;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;

public class FastUtilMapBasedRepository<T> implements InMemoryRepository<T> {

    private final Int2ObjectOpenHashMap<T> map;
    private int key;

    public FastUtilMapBasedRepository() {
        this.map = new Int2ObjectOpenHashMap<T>();
        this.key = 0;
    }

    @Override
    public void add(T object) {
        map.put(this.key, object);
        this.key += 1;
    }

    @Override
    public void remove(T object) {
        map.values().remove(object);
    }

    @Override
    public boolean contains(T object) {
        return map.containsValue(object);
    }
}