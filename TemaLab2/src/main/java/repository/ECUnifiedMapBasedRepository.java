package repository;

import org.eclipse.collections.impl.*;
import org.eclipse.collections.impl.map.mutable.UnifiedMap;

import java.util.Map;

public class ECUnifiedMapBasedRepository<T> implements InMemoryRepository<T>{
    private final Map<Integer, T> map;
    private int key;

    public ECUnifiedMapBasedRepository() {
        this.map = new UnifiedMap<Integer, T>();
        this.key = 0;
    }

    @Override
    public void add(T object) {
        this.map.put(this.key, object);
        this.key += 1;
    }

    @Override
    public boolean contains(T object) {
        return this.map.containsValue(object);
    }

    @Override
    public void remove(T object) {
        for (Map.Entry<Integer, T> entry : map.entrySet()){
            if(object.equals(entry.getValue())){
                map.remove(entry.getKey());
                break;
            }
        }
    }
}
