package repository;

import java.util.HashSet;
import java.util.Set;

public class HashSetBasedRepository<T> implements InMemoryRepository<T> {

    private final Set<T> set;

    public HashSetBasedRepository() {
        this.set = new HashSet<T>();
    }

    @Override
    public void add(T object) {
        this.set.add(object);
    }

    @Override
    public boolean contains(T object) {
        return this.set.contains(object);
    }

    @Override
    public void remove(T object) {
        this.set.remove(object);
    }
}
