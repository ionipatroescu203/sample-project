package model;

import java.util.Objects;

public class Order implements Comparable<Order> {
    private int id;
    private int price;
    private int quantity;

    public Order(int id, int price, int quantity) {
        this.id = id;
        this.price = price;
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return id == order.id && price == order.price && quantity == order.quantity;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, price, quantity);
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }


    @Override
    public int compareTo(Order that) {
        if (this.id != that.id) {
            return (this.id < that.id ? -1 : 1);
        }

        if (this.price != that.price) {
            return (this.price < that.price ? -1 : 1);
        }

        if (this.quantity != that.quantity) {
            return (this.quantity < that.quantity ? -1 : 1);
        }

        return 0;
    }
}
