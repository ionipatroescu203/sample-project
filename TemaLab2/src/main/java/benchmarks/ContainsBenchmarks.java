package benchmarks;

import model.Order;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import repository.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class ContainsBenchmarks {
    private static final Collection<Order> items = new ArrayList<>();

    public ContainsBenchmarks() {
        for(int i = 0; i < 1000; i++)
        {
            items.add(new Order(i, i, i));
        }
        Collections.shuffle((List<?>) items);
    }

    @State(Scope.Benchmark)
    public static class RepoState {

        ArrayListBasedRepository<Order> arrayListBasedRepository = new ArrayListBasedRepository<>();
        FastUtilMapBasedRepository<Order> fastUtilMapBasedRepository = new FastUtilMapBasedRepository<>();
        HashSetBasedRepository<Order> hashSetBasedRepository = new HashSetBasedRepository<>();
        KolobokeMapBasedRepository<Order> kolobokeMap = new KolobokeMapBasedRepository<>();
        TreeSetBasedRepository<Order> treeSet = new TreeSetBasedRepository<>();
        ConcurrentHashMapBasedRepository<Order> concurrentHashMap = new ConcurrentHashMapBasedRepository<>();
        ECUnifiedMapBasedRepository<Order> ecUnifiedMapBasedRepository = new ECUnifiedMapBasedRepository<>();

        @Setup(Level.Invocation)
        public void setup() {
            for (Order item : items) {
                arrayListBasedRepository.add(item);
                fastUtilMapBasedRepository.add(item);
                hashSetBasedRepository.add(item);
                kolobokeMap.add(item);
                treeSet.add(item);
                concurrentHashMap.add(item);
                ecUnifiedMapBasedRepository.add(item);
            }
        }

    }

    @Benchmark
    @BenchmarkMode({Mode.Throughput})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void arrayListBasedRepository(RepoState state) {
        for (Order item : items) {
            state.arrayListBasedRepository.contains(item);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.Throughput})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void fastUtilMapBasedRepository(RepoState state) {
        for (Order item : items) {
            state.fastUtilMapBasedRepository.contains(item);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.Throughput})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void hashSetBasedRepository(RepoState state) {
        for (Order item : items) {
            state.hashSetBasedRepository.contains(item);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.Throughput})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void kolobokeMap(RepoState state) {
        for (Order item : items) {
            state.kolobokeMap.contains(item);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.Throughput})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void treeSet(RepoState state) {
        for (Order item : items) {
            state.treeSet.contains(item);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.Throughput})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void concurrentHashMap(RepoState state) {
        for (Order item : items) {
            state.concurrentHashMap.contains(item);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.Throughput})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void ecUnifiedMapBasedRepository(RepoState state) {
        for (Order item : items) {
            state.ecUnifiedMapBasedRepository.contains(item);
        }
    }


    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(ContainsBenchmarks.class.getSimpleName())
//                .addProfiler(HotspotMemoryProfiler.class)
                .forks(1)
                .build();

        new Runner(opt).run();
    }

}