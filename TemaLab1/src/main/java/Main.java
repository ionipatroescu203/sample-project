import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String expression = reader.readLine();
            ExpressionParser expressionParser = new ExpressionParser();
            System.out.println(expressionParser.result(expression));
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
