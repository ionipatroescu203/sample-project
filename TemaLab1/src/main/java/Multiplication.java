public class Multiplication extends AbstractExpression {
    public Multiplication(Operation operation) {
        super(operation);
    }

    @Override
    public double executeOneOperation(double nr1, double nr2) {
        return nr1 * nr2;
    }
}
