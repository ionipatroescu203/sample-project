import java.util.Vector;

public class ExpressionFactory {

    private final static ExpressionFactory instance = new ExpressionFactory();

    private ExpressionFactory() {
    }

    public static ExpressionFactory getInstance() {
        return instance;
    }

    public AbstractExpression createExpression(Operation operation) {

        switch (operation) {
            case ADDITION:
                return new Addition(operation);
            case DIVISION:
                return new Division(operation);
            case SUBTRACTION:
                return new Subtraction(operation);
            case MULTIPLICATION:
                return new Multiplication(operation);
        }
        return null;

    }
}

