import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExpressionParser {

    public double result(String initialExpression) {

        if (initialExpression.matches("[.]*")) {
            throw new InvalidParameterException("Invalid expression!");
        } else {
            ExpressionFactory object = ExpressionFactory.getInstance();
            List<String> elements = new ArrayList<>(Arrays.asList(initialExpression.split(" ")));
            for (int i = 1; i < elements.size(); i += 2) {
                Operation operation = null;
                switch (elements.get(i)) {
                    case "/":
                        operation = Operation.DIVISION;
                        break;
                    case "*":
                        operation = Operation.MULTIPLICATION;
                        break;
                }

                if (operation != null) {
                    AbstractExpression abstractExpression = object.createExpression(operation);
                    double priorityResult = abstractExpression.execute(Double.parseDouble(elements.get(i - 1)), Double.parseDouble(elements.get(i + 1)));
                    elements.set(i - 1, String.valueOf(priorityResult));
                    elements.remove(i);
                    elements.remove(i);
                    i -= 2;
                }
            }

            double finalResult = Double.parseDouble(elements.get(0));
            for (int i = 1; i < elements.size(); i += 2) {
                Operation operation = null;
                switch (elements.get(i)) {
                    case "+":
                        operation = Operation.ADDITION;
                        break;
                    case "-":
                        operation = Operation.SUBTRACTION;
                        break;
                }
                AbstractExpression abstractExpression = object.createExpression(operation);
                finalResult = abstractExpression.execute(finalResult, Double.parseDouble(elements.get(i + 1)));
            }

            return finalResult;
        }
    }
}
