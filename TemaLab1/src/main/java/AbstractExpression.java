public abstract class AbstractExpression {
    private Operation operation;

    public AbstractExpression(Operation operation) {
        this.operation = operation;
    }

    public abstract double executeOneOperation(double nr1, double nr2);

    public final double execute(double nr1, double nr2) {
        return executeOneOperation(nr1, nr2);
    }
}
