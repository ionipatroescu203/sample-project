public class Division extends AbstractExpression {
    public Division(Operation operation) {
        super(operation);
    }

    @Override
    public double executeOneOperation(double nr1, double nr2) {
        return nr1 / nr2;
    }
}
